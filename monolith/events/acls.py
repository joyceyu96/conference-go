from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}

    url = "https://api.pexels.com/v1/search"

    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    response = requests.get(url, params=params, headers=headers)

    # converting our response back into python
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except Exception:
        return {"picture_url": None}

    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):

    # using the geo coding API to access lat and lon values
    geo_coding_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }

    geocoding = requests.get(geo_coding_url, params=params)
    content1 = json.loads(geocoding.content)
    try:
        lon = content1[0]["lon"]
        lat = content1[0]["lat"]
    except (KeyError, IndexError):
        return None

    # using the geocoding data to get the weather temp and description
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=params)
    content2 = json.loads(response.content)

    try:
        return {
            "temp": content2["main"]["temp"],
            "description": content2["weather"][0]["description"],
        }

    except Exception:
        return None

    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
